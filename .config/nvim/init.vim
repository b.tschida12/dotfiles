"=================================================="
"███╗   ██╗███████╗ ██████╗ ██╗   ██╗██╗███╗   ███╗"
"████╗  ██║██╔════╝██╔═══██╗██║   ██║██║████╗ ████║"
"██╔██╗ ██║█████╗  ██║   ██║██║   ██║██║██╔████╔██║"
"██║╚██╗██║██╔══╝  ██║   ██║╚██╗ ██╔╝██║██║╚██╔╝██║"
"██║ ╚████║███████╗╚██████╔╝ ╚████╔╝ ██║██║ ╚═╝ ██║"
"╚═╝  ╚═══╝╚══════╝ ╚═════╝   ╚═══╝  ╚═╝╚═╝     ╚═╝"
"==================================================
" Neovim configuration
set runtimepath^=~/.vim runtimepath+=/.vim/after
let &packpath = &runtimepath
source ~/.vimrc
" Plugins
call plug#begin('~/.vim/plugged')
    
    Plug 'tpope/vim-sensible'           " Better vim
    Plug 'airblade/vim-gitgutter'       " Show git diff in sign column
    Plug 'valloric/youcompleteme'       " Code completion
    Plug 'vim-airline/vim-airline'      " Status bar
    Plug 'sheerun/vim-polyglot'         " Language packs
    Plug 'tmhedberg/matchit'            " Better matching
    Plug 'dense-analysis/ale'           " Linting 
    Plug 'sirver/ultisnips'             " Snippets engine
    Plug 'honza/vim-snippets'           " Add snippets
    Plug 'plasticboy/vim-markdown'      " Markdown synax
    Plug 'yggdroot/indentline'          " Display vertical lines for indentation
    Plug 'vim-airline/vim-airline-themes' " Airline themes
    Plug 'lervag/vimtex'                " LaTeX plugin
    Plug 'wfxr/minimap.vim'             " Vim minimap
    Plug 'arcticicestudio/nord-vim'     " Nord colors


call plug#end()
" End plugins

set nocompatible                        " be iMproved, required
filetype off                            " required
filetype plugin on          
syntax enable
let g:vim_markdown_folding_disabled = 1
""""""""""""""
" Other Settings
"
""""""""""""""
set autoread                        " Reload nvim if changes made outside
set number relativenumber           " Use relative linenumbering
set nu rnu
set autoindent                      " Use previous line indents
set smartindent                     " Better autoindenting
set ignorecase                      " Ignore case when searching
set smartcase                       " 
set cursorline                      " Highlight current line
set tabstop=4                       "
set shiftwidth=4                    "
set softtabstop=4                   "
set expandtab                       " 
set showmatch                       "show matching brackets
""""""""""""""
"UI
""""""""""""""
set showcmd                         " Show previous commands
filetype indent on                  " Indenting filetypes
set wildmenu                        " Command autocompletion
set lazyredraw                      " Redraw only when neccesary
set showmatch                       " Highlight matching brackets
set wrap                            " Line wrapping
set title                           " Change window title
set spell                           " Enable spellcheck
set textwidth=80                    " Stop at 80 characters
set colorcolumn=+1                  "
set termguicolors                   " True colors
set mouse=a                         " Allow mouse support
colorscheme nord                    " Use nord colorscheme
""""""""""""""
" SEARCHING
""""""""""""""
set incsearch                       " Search as typed
set hlsearch                        " Highlight search results
set path+=**                        " Provide tab completion for file-related tasks


""""""""""""""
" Snippets
""""""""""""""
let g:UltiSnipsExpandTrigger="<alt>"
let g:UltiSnipsJumpForwardTrigger="<c-b>"
let g:UltiSnipsJumpBackwardTrigger="<c-z>"



""""""""""""""
" File Browsing
""""""""""""""
"Tweaks for browsing
" let g:netrw_banner=0            "Stops file banner
" let g:netrw_browse_split=4      "open in prior window
" let g:netrw_altv=1              "open splits to right
" let g:netrw_liststyle=3         "tree view
" let g:netrw_list_hide=netrw_gitignore#Hide()
" let g:netrw_list_hide=',\(^\|\s\s)\zs\.\S\+'

""""""""""""""
" Folding
""""""""""""""
set foldenable                      "enable folding
set foldlevelstart=10               " open most folds

""""""""""""""
" Minimap config
" github.com/wfxr/minimap.vim
""""""""""""""
let g:minimap_width = 10
let g:minimap_auto_start = 1
let g:minimap_auto_start_win_enter = 1



""""""""""""""
" Disable Arrow Keys
"
""""""""""""""

nnoremap <Up> <Nop>
nnoremap <Down> <Nop>
nnoremap <Left> <Nop>
nnoremap <Right> <Nop>
