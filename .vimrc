"
"
" ~/.vimrc
"
"
"
"

set nocompatible              " be iMproved, required
filetype off                  " required

" set the runtime path to include Vundle and initialize
set rtp+=~/.vim/bundle/Vundle.vim
call vundle#begin()
" alternatively, pass a path where Vundle should install plugins
"call vundle#begin('~/some/path/here')

" let Vundle manage Vundle, required
Plugin 'VundleVim/Vundle.vim'
Plugin 'morhetz/gruvbox'
Plugin 'w0rp/ale'
Plugin 'godlygeek/tabular'
Plugin 'plasticboy/vim-markdown'
Plugin 'sheerun/vim-polyglot'
Plugin 'tpope/vim-surround'
Plugin 'scrooloose/syntastic'
Plugin 'c.vim'
Plugin 'sjl/gundo.vim'
Plugin 'preservim/nerdcommenter'
Plugin 'lervag/vimtex'

" The following are examples of different formats supported.
" Keep Plugin commands between vundle#begin/end.
" plugin on GitHub repo
Plugin 'tpope/vim-fugitive'
" plugin from http://vim-scripts.org/vim/scripts.html
" Plugin 'L9'
" Git plugin not hosted on GitHub
Plugin 'git://git.wincent.com/command-t.git'
" git repos on your local machine (i.e. when working on your own plugin)

" The sparkup vim script is in a subdirectory of this repo called vim.
" Pass the path to set the runtimepath properly.
Plugin 'rstacruz/sparkup', {'rtp': 'vim/'}
" Install L9 and avoid a Naming conflict if you've already installed a
" different version somewhere else.
" Plugin 'ascenator/L9', {'name': 'newL9'}

" All of your Plugins must be added before the following line
call vundle#end()            " required
filetype plugin indent on    " required
" To ignore plugin indent changes, instead use:
filetype plugin on
"
" Brief help
" :PluginList       - lists configured plugins
" :PluginInstall    - installs plugins; append `!` to update or just :PluginUpdate
" :PluginSearch foo - searches for foo; append `!` to refresh local cache
" :PluginClean      - confirms removal of unused plugins; append `!` to auto-approve removal
"
" see :h vundle for more details or wiki for FAQ
" Put your non-Plugin stuff after this lin


colo gruvbox
syntax enable


let g:vim_markdown_folding_disabled = 1
""""""""""""""
" Other Settings
"
""""""""""""""

" reload vim if file is changed outside vim
set autoread
"Line Numbers
set number relativenumber
set nu rnu
"use previous line indentation
set autoindent
" better outoindent
set smartindent
"case when searching
set ignorecase
set smartcase
"highlight current line
set cursorline
"Tabs
set tabstop=4
set shiftwidth=4
set softtabstop=4
set expandtab
""""""""""""""
"UI
""""""""""""""
"Show previous command
set showcmd
" filetype indenting
filetype indent on
" autocomplete command line
set wildmenu
" redraw only when neccesary
set lazyredraw
" highlight matching brackets
set showmatch
" line wrapping
set wrap
" set window title
set title
" spellchecking
set spell

""""""""""""""
" SEARCHING
""""""""""""""
" search as typed
set incsearch
" highlight results
set hlsearch
" Provide tab completion for file-related tasks
set path+=**



""""""""""""""
" TAGS
""""""""""""""
" Create tags file
command! MakeTags !ctags -R .



""""""""""""""
" File Browsing
""""""""""""""
"Tweaks for browsing
" let g:netrw_banner=0            "Stops file banner
" let g:netrw_browse_split=4      "open in prior window
" let g:netrw_altv=1              "open splits to right
" let g:netrw_liststyle=3         "tree view
" let g:netrw_list_hide=netrw_gitignore#Hide()
" let g:netrw_list_hide=',\(^\|\s\s)\zs\.\S\+'

""""""""""""""
" Folding
""""""""""""""
"enable folding
set foldenable
" open most folds
set foldlevelstart=10


""""""""""""""
" Movement
"
""""""""""""""
"move by visual line
nnoremap j gj
nnoremap k gk
" move to beg/end line
" nnoremap B ^
" nnoremap E $
" Disable ^$
" nnoremap $ <nop>
" nnoremap ^ <nop>
" highlight last inserted line
nnoremap gV `[v`]


""""""""""""""
" Snippets
"
""""""""""""""





