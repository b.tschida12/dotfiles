# Dotfiles Repository


* .vim/
* .vimrc
* .config/nvim/
* .bashrc 
* .zshrc
* .zshrc.pre-oh-my-zsh
* .gitconfig
* .oh-my-zsh/
